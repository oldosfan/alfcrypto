#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "topaz.h"

//
// Context initialisation for the Topaz Crypto
//
void topazCryptoInit(TpzCtx *ctx, const unsigned char *key, int klen) {
   int i = 0; 
   ctx->v[0] = 0x0CAFFE19E;
    
   for (i = 0; i < klen; i++) {
      ctx->v[1] = ctx->v[0]; 
      ctx->v[0] = ((ctx->v[0] >> 2) * (ctx->v[0] >> 7)) ^  
                  (key[i] * key[i] * 0x0F902007);
   }
}

//
// decrypt data with the context prepared by topazCryptoInit()
//
    
void topazCryptoDecrypt(const TpzCtx *ctx, const unsigned char *in, unsigned char *out, int len) {
   unsigned int ctx1 = ctx->v[0];
   unsigned int ctx2 = ctx->v[1];
   int i;
   for (i = 0; i < len; i++) {
      unsigned char m = in[i] ^ (ctx1 >> 3) ^ (ctx2 << 3);
      ctx2 = ctx1;
      ctx1 = ((ctx1 >> 2) * (ctx1 >> 7)) ^ (m * m * 0x0F902007);
      out[i] = m;
   }
}


