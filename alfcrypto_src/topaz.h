#ifndef __TPZ_H
#define __TPZ_H

#include <stdio.h>

typedef struct _TpzCtx {
   unsigned int v[2];
} TpzCtx;

void topazCryptoInit(TpzCtx *ctx, const unsigned char *key, int klen);
void topazCryptoDecrypt(const TpzCtx *ctx, const unsigned char *in, unsigned char *out, int len);

#endif
