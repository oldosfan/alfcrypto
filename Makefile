# Special requirements for writing miniCode makefiles
# 1) Use the CC and CXX variables, and do *not* set them yourself
# 2) Only *append* CCFLAGS! Don't clear or rewrite the CCFLAGS variable
# 3) Use CCFLAGS or CXXFLAGS. Don't use your own variables.
# 4) gmake only understands indentation with tabs. Use the [[TAB]] button to input a tab (	) instead of spaces ( )
# 5) A target must be present named minicode, that calls all needed dependencies, or the build will fail
# 6) Don't tamper with the path setting
export PATH := /data/user/0/low.minicode/files/.res/aarch64/bin:$(PATH)

minicode: all

all: aes_cbc.o pukall.o topaz.o
	$(CC) $(CCFLAGS) -shared -Wall -fPIE aes_cbc.o pukall.o topaz.o -o libalfcrypto.so

aes_cbc.o:
	$(CC) $(CCFLAGS) -Wall -fPIE -c alfcrypto_src/aes_cbc.c -o aes_cbc.o

pukall.o: 
	$(CC) $(CCFLAGS) -Wall -fPIE -c alfcrypto_src/pukall.c -o pukall.o

topaz.o:
	$(CC) $(CCFLAGS) -Wall -fPIE -c alfcrypto_src/topaz.c -o topaz.o